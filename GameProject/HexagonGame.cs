﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.IO;
using System.Runtime.Serialization;

namespace GameProject
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class HexagonGame : Game
    {
        DataContractSerializer dataContractXMLSerializer;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Vector2 screenCenter;
        Vector2 triangleDirection;
        Random rnd = new Random();
        int hexagonPosition;

        float X0;   // Screen center 
        float Y0;   //
        float scale; //Enemy size
        float gameSpeed;
        float hexagonRotation;

        private const float delay = 5f;
        private float remainingDelay;
        private double seconds;

        private Texture2D textureBackground;
        private Texture2D textureTriangle;
        private Texture2D textureHexagonEnemy;
        private Texture2D textureHexagon;
        private SpriteFont font1;
        private SpriteFont font2;

        private Rotation rotation;
        private Triangle triangle;
        private Hexagon hexagon;
        private GameProcess gameProcess;
        string time;
        string loseText;
        string startText;
        string yourScore;
        string bestScoreText;
        int score;
        int bestScore;

        public HexagonGame()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width - 50;  // set this value to the desired width of your window
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height - 100;   // set this value to the desired height of your window
            graphics.ApplyChanges();
            this.IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            X0 = graphics.GraphicsDevice.Viewport.Bounds.Width / 2 ;
            Y0 = graphics.GraphicsDevice.Viewport.Bounds.Height / 2 ;
            triangleDirection = new Vector2(
                    X0,
                    Y0 - textureHexagon.Height / 4f);
            hexagon = new Hexagon(textureHexagon.Height / 2, textureHexagon.Width / 2, X0, Y0);
            triangle = new Triangle(textureTriangle.Height * 0.05f, textureTriangle.Width * 0.05f, triangleDirection);
            rotation = new Rotation(X0, Y0, textureHexagon.Height / 4f, triangle);
            hexagonPosition = rnd.Next(0, 6);
            screenCenter = new Vector2(X0, Y0);
            gameProcess = new GameProcess(rotation);
            loseText = "PRESS SPACEBAR TO START NEW GAME";
            startText = "PRESS SPACEBAR TO START";
            scale = 3;
            gameSpeed = 0.015f;
            remainingDelay = delay;

            score = 0;
            yourScore = "";
            bestScoreText = "";

            dataContractXMLSerializer = new DataContractSerializer(typeof(int));
            if (!File.Exists("BestScore.xml"))
            {
                using (FileStream fs = new FileStream("BestScore.xml", FileMode.Create))
                {
                    dataContractXMLSerializer.WriteObject(fs, 0);

                }
            }

            using (FileStream fs = new FileStream("BestScore.xml", FileMode.Open))
            {
                bestScore = (int)dataContractXMLSerializer.ReadObject(fs);
            }
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Content.RootDirectory = "Content/hex/";
            textureBackground = Content.Load<Texture2D>("back");
            textureTriangle = Content.Load<Texture2D>("triangle");
            textureHexagonEnemy = Content.Load<Texture2D>("hexagon");
            textureHexagon = Content.Load<Texture2D>("hexagonless");
            font1 = Content.Load<SpriteFont>("1");
            font2 = Content.Load<SpriteFont>("2");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// 
        
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            
            
            // TODO: Add your update logic here
            if (gameProcess.isGame)
            {
                if (!gameProcess.isLose)
                {
                    if (Keyboard.GetState().IsKeyDown(Keys.Right) || Mouse.GetState().RightButton == ButtonState.Pressed)
                    {
                        triangleDirection = rotation.RightRotation();
                    }

                    if (Keyboard.GetState().IsKeyDown(Keys.Left) || Mouse.GetState().LeftButton == ButtonState.Pressed)
                    {
                        triangleDirection = rotation.LeftRotation();
                    }
                    
                    //time = gameTime.TotalGameTime.Seconds - seconds + ":" + gameTime.TotalGameTime.Milliseconds;

                    var timer = (float)gameTime.ElapsedGameTime.TotalSeconds;
                    remainingDelay -= timer;
                    score += 1;
                    

                    if (score > bestScore)
                    {
                        bestScore = score;
                    }


                    if (remainingDelay <= 0)
                    {
                        gameSpeed += 0.001f;
                        remainingDelay = delay;
                    }

                    scale -= gameSpeed;
                    hexagonRotation += (float)Math.PI / 180;

                    if (scale <= 1.1f && scale > 0.5)
                    {
                        yourScore = "YOUR SCORE:" + score;
                        bestScoreText = "BEST SCORE:" + bestScore;
                        gameProcess.CheckLose(hexagonPosition);
                    }
                    else if (scale <= 0.5)
                    {
                        scale = 3;
                        hexagonPosition = rnd.Next(0, 6);
                    }
                }
                else
                {
                    using (FileStream fs = new FileStream("BestScore.xml", FileMode.Create))
                    {
                        dataContractXMLSerializer.WriteObject(fs, bestScore);

                    }
                    score = 0;
                    seconds = gameTime.TotalGameTime.Seconds;
                    hexagonPosition = rnd.Next(0, 6);
                    if (Keyboard.GetState().IsKeyDown(Keys.Space))
                    {
                        triangleDirection = new Vector2(
                            X0,
                            Y0 - textureHexagon.Height / 4f);
                        triangle = new Triangle(textureTriangle.Height * 0.05f, textureTriangle.Width * 0.05f, triangleDirection);
                        rotation = new Rotation(X0, Y0, textureHexagon.Height / 4f, triangle);
                        gameProcess = new GameProcess(rotation);
                        gameProcess.isGame = true;
                        gameSpeed = 0.015f;
                        scale = 3;

                        gameProcess.isLose = false;

                    }
                }
            }
            else
            {
                seconds = gameTime.TotalGameTime.Seconds;
                hexagonRotation += (float)Math.PI / 180;
                triangleDirection = rotation.AutoRotation(Math.PI);
                if (Keyboard.GetState().IsKeyDown(Keys.Space))
                {
                    time = triangle.B1.ToString();
                    gameProcess.isGame = true;
                }
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
            if (gameProcess.isGame)
            {
                if (!gameProcess.isLose)
                {
                    spriteBatch.Draw(textureHexagon, 
                        screenCenter, 
                        null, 
                        Color.White, 
                        hexagonRotation, 
                        hexagon.HexagonCenter, 
                        0.5f, 
                        SpriteEffects.None, 
                        1f);
                    spriteBatch.Draw(textureHexagonEnemy, 
                        screenCenter, 
                        null, 
                        Color.White, 
                        0.0174533f * (30 + hexagonPosition * 60), 
                        hexagon.HexagonCenter, 
                        scale, 
                        SpriteEffects.None, 
                        1f);
                    spriteBatch.Draw(textureTriangle, 
                        triangleDirection, 
                        null, 
                        Color.White, 
                        (float)rotation.angle, 
                        triangle.TriangleCenter,
                        0.05f,
                        SpriteEffects.None,
                        1f);
                    spriteBatch.DrawString(font2,
                        "Current score:" + score, 
                        new Vector2(100, 100), 
                        Color.Black);
                    spriteBatch.DrawString(font2,
                        "Best score:" + bestScore,
                        new Vector2(100, 200),
                        Color.Black);
                }
                else
                {
                    spriteBatch.DrawString(font1, loseText, new Vector2(X0, Y0 + 30), Color.Black, 0, font1.MeasureString(loseText) / 2, 1f, SpriteEffects.None, 1f);
                    spriteBatch.DrawString(font1, bestScoreText, new Vector2(X0, Y0 - 10), Color.Black, 0, font1.MeasureString(bestScoreText) / 2, 1f, SpriteEffects.None, 1f);
                    spriteBatch.DrawString(font1, yourScore, new Vector2(X0, Y0 + 70), Color.Black, 0, font1.MeasureString(yourScore) / 2, 1f, SpriteEffects.None, 1f);
                }
            }
            else
            {
                spriteBatch.Draw(textureHexagon, screenCenter, null, Color.White, hexagonRotation, hexagon.HexagonCenter, 0.5f, SpriteEffects.None, 1f);
                spriteBatch.Draw(textureTriangle, triangleDirection, null, Color.White, (float)rotation.angle, triangle.TriangleCenter, 0.05f, SpriteEffects.None, 1f);
                spriteBatch.DrawString(font1, startText, new Vector2(X0, Y0), Color.Black, 0, font1.MeasureString(startText) / 2, 1f, SpriteEffects.None, 1f);
            }

            spriteBatch.End();
        }
    }
}