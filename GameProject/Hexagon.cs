﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameProject
{
    class Hexagon
    {
        public Vector2 A { get; private set; }
        public Vector2 B { get; private set; }
        public Vector2 C { get; private set; }
        public Vector2 D { get; private set; }
        public Vector2 E { get; private set; }
        public Vector2 F { get; private set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public Vector2 HexagonCenter;

        public Hexagon() { }

        public Hexagon(float height, float width, float X0, float Y0)
        {
            Height = height;
            Width = width;
            A = new Vector2(X0 - Height / 4, Y0 - Width / 2);
            B = new Vector2(X0 + Height / 4, Y0 - Width / 2);
            C = new Vector2(X0 + Height / 2, Y0);
            D = new Vector2(X0 + Height / 4, Y0 + Width / 2);
            E = new Vector2(X0 - Height / 4, Y0 + Width / 2);
            F = new Vector2(X0 - Height / 2, Y0);
            HexagonCenter = new Vector2(Width, Height);
        }
    }
}
