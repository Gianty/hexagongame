﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameProject
{
    class GameProcess
    {
        public bool isLose;
        public bool isGame;
        public bool isPause;

        public Vector2 A { get; private set; }
        public Vector2 B { get; private set; }
        public Vector2 C { get; private set; }
        public Vector2 D { get; private set; }
        public Vector2 E { get; private set; }
        public Vector2 F { get; private set; }

        Rotation rotation;

        public GameProcess(Rotation _rotation)
        {
            isLose = false;
            isGame = false;
            isPause = false;
            rotation = _rotation;
            MakePoints();
        }

        public void MakePoints()
        {
            double rad = Math.PI / 180;
            A = rotation.PointRotation(rotation.Radius + rotation.triangle.Height, rad * 330);
            B = rotation.PointRotation(rotation.Radius + rotation.triangle.Height, rad * 30);
            C = rotation.PointRotation(rotation.Radius + rotation.triangle.Height, rad * 90);
            D = rotation.PointRotation(rotation.Radius + rotation.triangle.Height, rad * 150);
            E = rotation.PointRotation(rotation.Radius + rotation.triangle.Height, rad * 210);
            F = rotation.PointRotation(rotation.Radius + rotation.triangle.Height, rad * 270);
        }
        
        public void CheckLose(int posHex)
        {
            float X = rotation.triangle.B1.X;
            float Y = rotation.triangle.B1.Y;

            if (posHex == 0)
            {
                if (!(X > A.X  && Y < A.Y && X < B.X && Y < B.Y))
                    isLose = true;
            }
            else if (posHex == 1)
            {
                if (!(X > B.X && Y > B.Y && X < C.X && Y < C.Y))
                    isLose = true;
            }
            else if (posHex == 2)
            {
                if (!(X < C.X && Y > C.Y && X > D.X && Y < D.Y))
                    isLose = true;
            }
            else if (posHex == 3)
            {
                if (!(X < D.X && Y > D.Y && X > E.X && Y > E.Y))
                    isLose = true;
            }
            else if (posHex == 4)
            {
                if (!(X < E.X && Y < E.Y && X > F.X && Y > F.Y))
                    isLose = true;
            }
            else if (posHex == 5)
            {
                if (!(X > F.X && Y < F.Y && X < A.X && Y > A.Y))
                    isLose = true;
            }
        }



    }
}
