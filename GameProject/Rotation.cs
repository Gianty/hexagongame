﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameProject
{
    class Rotation
    {
        public float X0 { get; private set; }
        public float Y0 { get; private set; }
        public double angle;
        public const double rad = Math.PI/180;
        public float Radius;
        public Triangle triangle;

        public Rotation()
        {
           
        }

        public Rotation(float x0, float y0, float radius, Triangle trian)
        {
            X0 = x0;
            Y0 = y0;
            triangle = trian;
            Radius = radius;
            angle = 0;
        }

        public Vector2 LeftRotation()
        {
            angle -= rad * 5;
            triangle.B1 = PointRotation(Radius + triangle.Height, angle);
            return PointRotation(Radius, angle); 
        }

        public Vector2 RightRotation()
        {
            angle += rad * 5;
            triangle.B1 = PointRotation(Radius + triangle.Height, angle);
            return PointRotation(Radius, angle); 
        }

        public Vector2 AutoRotation(double a)
        {
            angle += a * rad;
            triangle.B1 = PointRotation(Radius + triangle.Height, angle);
            return PointRotation(Radius, angle);
        }

        public float Sin(double Angle)
        {
            return (float)Math.Sin(Angle);
        }

        public float Cos(double Angle)
        {
            return (float)Math.Cos(Angle);
        }

        public Vector2 PointRotation(float radius, double Angle)
        {
            return new Vector2(X0 + radius * Sin(Angle), Y0 - radius * Cos(Angle));

        }


    }
}
