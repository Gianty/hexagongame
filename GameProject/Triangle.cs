﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameProject
{
    class Triangle
    {
        //public Vector2 A { get; set; }
        public Vector2 B { get; set; } //Координаты вершины B при запуске игры
        //public Vector2 C { get; set; }
        //public Vector2 A1 { get; set; }
        public Vector2 B1 { get; set; } //Текущие координаты точки B
        //public Vector2 C1 { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public Vector2 TriangleCenter;

        Triangle() { }
         
        public Triangle(float height, float width, Vector2 tD)
        {
            Height = height;
            Width = width;
            TriangleCenter = new Vector2(Width / 0.05f / 2, Height / 0.05f);
            //A = new Vector2(tD.X - Width / 2, tD.Y );
            B = new Vector2(tD.X, tD.Y - Height);
            //C = new Vector2(tD.X + Width / 2, tD.Y );
            //A1 = A;
            B1 = B;
            //C1 = C;
        }
    }
}
